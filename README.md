# multi-install-mysql

## 项目介绍
```
linux 下安装 mysql 数据库的 bash 脚本。
跟进 MySQL 和 Percona 官方版本进行脚本更新。
```

## 使用说明
```
脚本文件: multi-install-mysql.sh
脚本功能: Unix/Linux 下安装二进制版 MySQL/Percona Server For MySQL
脚本用法: bash|bin/bash multi-install-mysql.sh [source|install|remove]
系统支持: AMD64 + Linux + Bash
版本要求: 5.5.x >= 5.5.8 | 5.6.x >= 5.6.10 | 5.7.x >= 5.7.9 | 8.0.x >= 8.0.11
预置命令: bash xz nc rar rar_static

将从官方下载的二进制 tar.gz 或 tar.xz 安装包文件放在脚本同级目录，运行脚本即可。
bin 目录下的预置命令文件是为了解决在较老的OS上命令文件版本过低导致脚本运行异常的问题。

```

## 参考命令
```shell
# 安装
bash multi-install-mysql.sh install
```
```shell
# 卸载
bash multi-install-mysql.sh remove
```


## 注意事项
```
1、只支持 amd64 平台架构。
2、只支持 二进制解压版。
```

## 其他信息
```
MySQL官网: https://www.mysql.com
Percona官网: https://www.percona.com
```