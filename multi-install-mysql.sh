#!/usr/bin/env bash
#
# 文 件 名: multi-install-mysql.sh
# 脚本功能: linux下安装官方MySQL数据库
# 文件编码: UTF-8
# 编程语言: Shell Script(Bash 4.x/5.x)
# 编程工具: VSCode/Goland
# 显示语言: 简体中文
# 脚本编写：杨芳超
# 联系邮箱: i-up@qq.com
# 问题反馈: https://seeyon.ren
#

# ******************** 按需配置以下参数 ******************** #

# 软件安装 '根' 目录
installRootDir="/apps"

# 是否载入脚本所在目录下的bin目录 1=加载
# 此参数主要用于老系统的bash版本过低时 使用预置的命令文件来运行 仅虚拟化平台 物理机可能会报段错误
loadScriptBinDir=""

# 如果已存在同名安装目录 是否开启备份 1=开启 只支持 tgz|rar|zip 格式 备份目录默认为脚本所在平级的 backup 目录
# 建议开启此参数 防止误删 脚本未做人工选择提示 将直接执行任务
# 注意: 此备份是备份 脚本设定的 mysql 的安装文件夹 不是数据库数据的导出导入那种备份
enableBackup="1"
# 备份文件保存目录 默认保存在脚本所在目录下的backup下 若需修改 请设置为绝对路径
backupDir="$(dirname "$(readlink -f "$0")")/backup"
# 备份文件格式(后缀名) 只能是 tgz|rar|zip
backupFormat="zip"
# 客户端和服务端 当前设置为相同 端口默认为 3306 在同一台主机安装多个版本的时候 注意修改端口号和安装路径以及启动控制文件
dbPort="3306"
# 字符集设置(仅字符集 不设置字符序) 如果设置为 utf8mb4 请使用 mysql 5.7/8.0
dbCharacter="utf8"
# 默认存储引擎
dbEngine="InnoDB"
# 初始随机密码位数 默认为 10 位 10 <= dbPWDDigit < 30
dbPWDDigit="10"
# 身份认证插件 仅安装 MySQL 8.x 时按需设置 caching_sha2_password | mysql_native_password | sha256_password
# 8.0.27 开始使用 authentication_policy 并将要废弃 default_authentication_plugin
dbDefaultAuthPlugin="mysql_native_password"
# 数据库最大连接数
dbMaxConnect="300"

# max_allowed_packet 参数值大小 单位MB 此参数服务端和客户端都有
# 当迁移/升级OA数据库数据时 怀疑 blob 过大导致异常时 可考虑在文件 bin/mysqld_safe 中添加: ulimit -d 256000 并重启 mysqld 服务
# https://dev.mysql.com/doc/refman/8.0/en/packet-too-large.html
dbMaxAllowedPacket="256"

# innodb_buffer_pool_size 参数值占用的的内存百分比 脚本将根据这里指定的百分比进行计算和取整赋值
# 计算公式为 总内存大小(GB)*1024*内存百分比=innodb_buffer_pool_size(MB)
# 需指定为1位数的小数点 如 0.6 表示 60% 即 innodb_buffer_pool_size 的值 = 物理内存的 60%
dbIBPSizeUseMemPercent="0.4"

# 创建数据库 不需创建则双引号内保留为空 数据库名不要使用空格等特殊字符 正确样例: createDBName="a8v81"
createDBName=""

# MySQL 8.0.x 时 为了避免 binlog 撑爆磁盘 设置清除过期日志的时间 以秒为单位
dbBinLogExpireLimit="86400"

# MySQL 默认时区 国内用户使用 8.0+ 时需要调整此参数
dbDefaultTimeZone="+8:00"

# MySQL 事务隔离级别 READ-UNCOMMITTED | READ-COMMITTED | REPEATABLE-READ | SERIALIZABLE
dbTransactionIsolation="READ-COMMITTED"

# innodb 数据文件及 redo log 的打开、刷写模式 只能是 fdatasync(默认) | O_DSYNC | O_DIRECT
# fdatasync，调用 fsync() 去刷数据文件与 redo log 的 buffer
# O_DSYNC innodb 会使用 O_SYNC 方式打开和刷写 redo log 使用 fsync() 刷写数据文件
# O_DIRECT innodb 使用 O_DIRECT 打开数据文件 使用 fsync() 刷写数据文件跟 redo log
innodbFlushMethod="O_DIRECT"

# innodb_page_size 大小值(单位为K) 官方默认是16k 最好是根据磁盘系统和性能来设置
innodbPageSize="16"

# slow_query_log 根据需要设置开启慢查询日志 只能是 ON|OFF 其中 ON=开启 OFF=关闭
slowQueryLog="OFF"

# 是否加入开机自启动 1=启用 注意: 仅支持 systemd 服务管理方式
enableAutostart="1"
# 安装后是否启动服务 1=启动
startAppSrv="1"

# 是否写入 PATH 系统环境变量 1="写入"
enableWriteEnvPATH="1"
# 是否写入 LD_LIBRARY_PATH 系统环境变量 1=写入
enableWriteEnvLD=""
# 系统环境变量配置文件
sysEnvFile="/etc/profile"

# **** 以下参数不建议修改 **** #
appInstallDir="${installRootDir}/mysql"
appBinDir="${installRootDir}/mysql/bin"
appLogDir="${installRootDir}/mysql/logs"
appDataDir="${installRootDir}/mysql/data"
appLibDir="${installRootDir}/mysql/lib"
# appPidFile="${appDataDir}/mysqld.pid"
appSbinFile="${appBinDir}/mysqld"
appBinFile="${appBinDir}/mysql"
appAdminBinFile="${appBinDir}/mysqladmin"
# mysql 配置文件
appEtcFile="${appInstallDir}/my.cnf"
# mysql 错误日志文件
appErrLogFile="${appLogDir}/error.log"
sockFileServer="${appDataDir}/mysql.sock"
sockFileClient="${appDataDir}/mysql.sock"
sockFileMysql="${appDataDir}/mysql.sock"
# appEnvFile="${appInstallDir}/support-files/mysql.env"
appSrvFile="${appInstallDir}/support-files/mysql.server"
# 服务控制文件 - systemd
sysSrvFile="/etc/systemd/system/mysqld.service"
# **** 以上参数不建议修改 **** #

# ******************** 按需配置以上参数 ******************** #

# 自定义颜色函数
function oput() {
    # 功能: 自定义颜色并打印标准输出
    # 例子: oput red "系统依赖检查未通过"
    local red_color green_color yellow_color blue_color pink_color white_blue down_blue flash_red res
    # 红色
    red_color='\E[1;31m'
    # 绿色
    green_color='\E[1;32m'
    # 黄色
    yellow_color='\E[1;33m'
    # 蓝色
    blue_color='\E[1;34m'
    # 紫色
    pink_color='\E[1;35m'
    # 白底蓝字
    white_blue='\E[47;34m'
    # 下划线+蓝色
    down_blue='\E[4;36m'
    # 红闪
    flash_red='\E[5;31m'
    # 重置
    res='\E[0m'

    # 限制传入参数必须为2个 否则退出
    if [ $# -ne 2 ]; then echo "Usage $0 {red|yellow|blue|green|pink|wb|db|fr} content"; exit; fi
    case "$1" in
        red | RED ) echo -e "${red_color}$2${res}"; ;;
        yellow | YELLOW ) echo -e "${yellow_color}$2${res}"; ;;
        green | GREEN ) echo -e "${green_color}$2${res}"; ;;
        blue | BLUE ) echo -e "${blue_color}$2${res}"; ;;
        pink | PINK ) echo -e "${pink_color}$2${res}"; ;;
        wb | WB ) echo -e "${white_blue}$2${res}"; ;;
        db | DB ) echo -e "${down_blue}$2${res}"; ;;
        fr | FR ) echo -e "${flash_red}$2${res}"; ;;
        * ) echo -e "请指定一个颜色代码：{red|yellow|blue|green|pink|wb|db|fr}"; ;;
    esac
}

# 语言环境
export LANG=zh_CN.UTF-8
# PATH设置
export PATH=/sbin:/bin:/usr/sbin:/usr/bin
# 脚本目录 -- 与工作目录有区别
scriptDir="$(dirname "$(readlink -f "$0")")"
if ! cd "${scriptDir}"; then oput red "目录切换: 失败 ${scriptDir}" && exit; fi
# 脚本文件
scriptFile="$(basename "$0")"
# 脚本功能
scriptDesc="Linux 下安装 MySQL/Percona 官方二进制版"
# 系统环境
scriptEnv="Linux + Bash 4.x/5.x + Systemd"
# Bin 目录
scriptBinDir="${scriptDir}/bin"
# Log 目录
scriptLogDir="${scriptDir}/logs"
if ! mkdir -p "${scriptLogDir}"; then echo "Failed to create script log directory" && exit; fi
# Log 文件
scriptLogFile="${scriptLogDir}/script.log"
# 脚本用法
scriptUsage="bash ${scriptFile} [install|remove|backup|depend]"
# 创建时间
# scriptCreate="2014-06-02 08:41:38"
# 更新时间
scriptUpdate="2022-09-27 20:28:18"

# 操作系统
OS="$(uname | tr '[:upper:]' '[:lower:]')"
if [ "${OS}" != "linux" ]; then echo "${OS} is not supported by this script" && exit; fi

# 平台架构 目前还没看到官方有 arm64 的二进制解压包 所以当前脚本限制为 x86_64/amd64
archType="$(uname -m)"
case "${archType}" in
    "x86_64" | "amd64" )
        ;;
    * )
        echo; oput red "${archType} is not supported by this script"; echo; exit
        ;;
esac

# 帐号权限
if [ "$(id -un)" != "root" ]; then echo "需要使用 root 权限运行脚本" && exit; fi

# bash版本
if [ "${BASH_VERSION:0:1}" -lt 4 ]; then echo "Bash 版本太低" && exit; fi

# 服务管理 - 脚本设定只支持使用 systemd 进行服务管理
if command -v systemctl &>/dev/null; then
    # initIs 表示 Linux 系统的 init 分类
    initIs="systemd"
    # srvCtlCMD 服务管理控制命令
    srvCtlCMD="systemctl"
else
    initIs="unkown"
    srvCtlCMD="unkown"
fi

# 生成并清空脚本运行日志
touch "${scriptLogFile}" && true >"${scriptLogFile}"
# 写入当前系统时间和脚本文件名到脚本日志
(echo; echo "$(date '+%Y-%m-%d %H:%M:%S') from ${scriptDir}/${scriptFile}") >>"${scriptLogFile}"

# IP地址 多IP时 只取第一个IP 自动获取 如有需要请手动指定 如 hostAddr="192.168.100.111"
hostAddr="$(LANG=en_US.UTF-8 ip addr | grep -Po '(?<=inet ).*(?=\/)' | grep -v '127.0.0' | head -1)"
# 客户端IP地址 - 自动获取
clientAddr="$(who -u am i 2>/dev/null | awk '{print $NF}'| sed -e 's/[a-zA-Z:()]//g')"

# 打印脚本信息
function print_parameter_info() {
    echo "软件安装目录: ${installRootDir}"
    echo "软件备份目录: ${backupDir}"
    echo "备份文件格式: ${backupFormat}"
    if [ "${enableBackup}" = "1" ]; then
        echo "软件自动备份: 已启用"
    else
        echo "软件自动备份: 未启用"
    fi

    echo -e "系统环境变量: \c"
    if [ "${enableWriteEnvPATH}" = "1" ]; then
        echo -e "写入 PATH 到 ${sysEnvFile}; \c"
    else
        echo -e "不写入 PATH; \c"
    fi
    if [ "${enableWriteEnvLD}" = "1" ]; then
        echo "写入 LD_LIBRARY_PATH 到 ${sysEnvFile}"
    else
        echo "不写入 LD_LIBRARY_PATH"
    fi
    if [ "${enableAutostart}" = "1" ]; then
        echo "设置开机自启: 是"
    else
        echo "设置开机自启: 否"
    fi
    if [ "${startAppSrv}" = "1" ]; then
        echo "装完启动服务: 是"
    else
        echo "装完启动服务: 否"
    fi

    echo "服务管理系统: ${initIs}"
    echo "服务控制命令: ${srvCtlCMD}"

    if [ "${createDBName}" != "" ]; then echo "创建新数据库: ${createDBName}"; fi
}

# 脚本参数校验
function verify_scriptPara() {
    if [ "${loadScriptBinDir}" = "1" ]; then
        if [ -d "${scriptBinDir}" ]; then chmod -R +x "${scriptBinDir}" && export PATH="${scriptBinDir}:$PATH"; fi
    fi

    if [ "${installRootDir}" = "" ]; then
        oput red "未设置软件安装根目录" && return 1
    elif ! echo "${installRootDir}" | grep -Eq '^/[a-zA-Z]'; then
        oput red "需要设置软件安装根目录为绝对路径: 例如 /apps" && return 1
    fi
    if [ ! -d "${installRootDir}" ]; then
        echo -e "创建目录: ${installRootDir} \c"
        if ! mkdir -p "${installRootDir}"; then
            oput red "失败" && return 1
        else
            oput green "成功"
        fi
    fi

    # 如果开启备份 则创建备份目录
    if [ "${enableBackup}" = "1" ]; then mkdir -p "${backupDir}"; fi
    case "${backupFormat}" in
        "tgz" ) packProg="tar"; ;;
        "rar" ) packProg="rar"; ;;
        "zip" ) packProg="zip"; ;;
        "7z" ) packProg="7zzs"; ;;
        * ) echo; oput red "备份格式: 错误 只支持 tgz/rar/zip/7z"; echo && return 1; ;;
    esac

    if ! echo "${dbIBPSizeUseMemPercent}" | grep -Eq '^0.[1-9]$'; then
        oput red "参数有误: dbIBPSizeUseMemPercent 的值必须是 0. 开头且只保留 1 位小数点的浮点数 如 0.6"
        return 1
    fi
}

# 检查脚本所需命令
function check_CMD() {
    local CMDLists=(getconf pgrep ss tee)
    if [ "${enableBackup}" = "1" ]; then CMDLists+=("${packProg}"); fi
    local CMD
    for CMD in "${CMDLists[@]}"; do
        if ! command -v "${CMD}" &>/dev/null; then
            echo; oput red "所需命令: ${CMD} 未找到 请参考如下命令执行安装"
            echo "RedHat系操作系统 请使用 yum install -y ${CMD}"
            echo "Debian系操作系统 请使用 apt install -y ${CMD}"
            main_prompt
            return 1
        fi
    done
}

# 禁用 selinux
function disable_selinux()
{
    # 禁用 selinux -- 永久禁用的时候需要重启 这东东经常莫名其妙的导致编译问题或服务启动问题
    if [ -s /etc/selinux/config ]; then
        if grep -q 'SELINUX=enforcing' /etc/selinux/config; then
            sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
        fi
    fi
    setenforce 0 &>/dev/null
}

function create_groupAndUser() {
    local sysGroupName sysUserName grouadd_cmd useradd_cmd
    sysGroupName="mysql"
    sysUserName="mysql"
    grouadd_cmd="groupadd ${sysGroupName}"
    useradd_cmd="useradd -r -g ${sysGroupName} -s /bin/false ${sysUserName}"
    echo -e "创建系 统 组: ${sysGroupName} \c"
    # 判断组是否存在
    if ! grep -Eq "^${sysGroupName}" /etc/group; then
        if eval "${grouadd_cmd}" &>>"${scriptLogFile}"; then
            oput green "成功"
        else
            oput red "失败"
            return 1
        fi
    else
        oput yellow "已存在 无需再次创建"
    fi
    echo -e "创建系统用户: ${sysUserName} \c"
    # 判断用户是否存在
    if ! grep -Eq "^${sysUserName}" /etc/passwd; then
        if eval "${useradd_cmd}" &>>"${scriptLogFile}"; then
            oput green "成功"
        else
            oput red "失败"
            return 1
        fi
    else
        oput yellow "已存在 无需再次创建"
    fi
}

# 根据不同服务管理方式创建不同的服务控制文件
function create_sysSrvFile() {
    echo -e "创建服务文件: ${sysSrvFile} \c"
    if [ "${initIs}" != "systemd" ]; then
        oput red "失败 当前系统不支持 systemd"
        return 1
    fi

    # 如果服务控制文件存在 则备份
    if [ -f "${sysSrvFile}" ]; then
        cp "${sysSrvFile}" "${sysSrvFile}-$(date '+%Y%m%d%H%M%S')"
    fi

    cat >"${sysSrvFile}" <<EOF
[Unit]
Description=MySQL Server
Documentation=man:mysqld(8)
Documentation=https://dev.mysql.com/doc/refman/en/using-systemd.html
After=network.target syslog.target
[Install]
WantedBy=multi-user.target
[Service]
User=mysql
Group=mysql
Type=forking
# Disable service start and stop timeout logic of systemd for mysqld service.
TimeoutSec=0
# Execute pre and post scripts as root
PermissionsStartOnly=true
# Start main service
ExecStart=${appSrvFile} start
# Sets open_files_limit
LimitNOFILE = 65535
Restart=on-failure
RestartPreventExitStatus=1
# Set environment variable MYSQLD_PARENT_PID. This is required for restart.
Environment=MYSQLD_PARENT_PID=1
PrivateTmp=false
EOF

    if [ -f "${sysSrvFile}" ]; then
        oput green "成功"
    else
        oput red "失败"
        return 1
    fi
    systemctl daemon-reload
}

# 设置软件运行参数
function set_appPara() {
    local dbIBPS
    dbIBPS="$(awk "BEGIN {print ${memTotal}*1024*${dbIBPSizeUseMemPercent}}")"
    echo -e "创建配置文件: ${appEtcFile} \c"
    case "${tarFileMajorVer}.${tarFileMinorVer}" in
        "5.5" )
            # 安装 mysql 5.5.x 的代码段
            cat >"${appEtcFile}" <<EOF
[client]
port=${dbPort}
default-character-set=${dbCharacter}
socket=${sockFileClient}

[mysql]
default-character-set=${dbCharacter}
socket=${sockFileMysql}

[mysqld]
port=${dbPort}
character-set-server=${dbCharacter}
basedir=${appInstallDir}
datadir=${appDataDir}
user=mysql
slow_query_log=${slowQueryLog}
lower_case_table_names=1
# skip-name-resolve
socket=${sockFileServer}
default-storage-engine=${dbEngine}
max_connections=${dbMaxConnect}
# max_connect_errors=100
max_allowed_packet=${dbMaxAllowedPacket}M
# innodb_log_files_in_group=3
innodb_log_file_size=256M
innodb_log_buffer_size=512M
innodb_buffer_pool_size=${dbIBPS%.*}M
# innodb_additional_mem_pool_size 和 innodb_use_sys_malloc 在 MySQL 5.7.4 中移除
innodb_additional_mem_pool_size=16M
innodb_flush_method=${innodbFlushMethod}
innodb_lock_wait_timeout=1800
innodb_flush_log_at_trx_commit=0
innodb_autoextend_increment=64
# innodb_lru_scan_depth=256
# innodb_io_capacity=
# innodb_io_capacity_max=
# 内存缓冲池个数 即控制并发读写 innodb_buffer_pool 的个数
# innodb_buffer_pool_instances=CPU核数
# innodb_read_io_threads=64
# innodb_write_io_threads=64
# innodb_open_files=300
# innodb_file_per_table=1
# innodb_file_format=Barracuda
innodb_page_size=${innodbPageSize}K
# innodb_thread_concurrency=并发线程数(官方推荐CPU核数的2倍)
# sync_binlog=0
group_concat_max_len=1024000
transaction-isolation=${dbTransactionIsolation}
default-time-zone='${dbDefaultTimeZone}'
# query_cache_limit 在 MySQL 8.0.3 移除
query_cache_limit=2M
# query_cache_size 在 MySQL 8.0.3 移除
query_cache_size=64M
tmp_table_size=512M
thread_cache_size=32
# key_buffer_size 仅对 myisam 有效
key_buffer_size=256M
read_buffer_size=1M
read_rnd_buffer_size=128M
sort_buffer_size=1M
sql_mode="NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES"
# 安全加固
skip_symbolic_links=0
local-infile=0
# connect_timeout=60

[mysqld_safe]
log-error=${appErrLogFile}

[mysql.server]
basedir=${appInstallDir}
EOF
            ;;
        "5.6" )
            # 安装 mysql 5.6.x 的代码段
            cat > "${appEtcFile}" <<EOF
[client]
port=${dbPort}
default-character-set=${dbCharacter}
socket=${sockFileClient}

[mysql]
default-character-set=${dbCharacter}
socket=${sockFileMysql}

[mysqld]
port=${dbPort}
character-set-server=${dbCharacter}
basedir=${appInstallDir}
datadir=${appDataDir}
user=mysql
slow_query_log=${slowQueryLog}
lower_case_table_names=1
# skip-name-resolve
socket=${sockFileServer}
default-storage-engine=${dbEngine}
max_connections=${dbMaxConnect}
# max_connect_errors=100
max_allowed_packet=${dbMaxAllowedPacket}M
# innodb_log_files_in_group=3
innodb_log_file_size=256M
innodb_log_buffer_size=512M
innodb_buffer_pool_size=${dbIBPS%.*}M
# innodb_additional_mem_pool_size 和 innodb_use_sys_malloc 在 MySQL 5.7.4 中移除
innodb_additional_mem_pool_size=16M
innodb_flush_method=${innodbFlushMethod}
innodb_lock_wait_timeout=1800
innodb_flush_log_at_trx_commit=1
innodb_autoextend_increment=64
# innodb_lru_scan_depth=256
# innodb_io_capacity=
# innodb_io_capacity_max=
# innodb_buffer_pool_instances=CPU核数
# innodb_read_io_threads=64
# innodb_write_io_threads=64
# innodb_open_files=300
innodb_file_per_table=1
# innodb_file_format=Barracuda
innodb_page_size=${innodbPageSize}K
# innodb_thread_concurrency=并发线程数(官方推荐CPU核数的2倍)
# sync_binlog=0
group_concat_max_len=1024000
transaction-isolation=${dbTransactionIsolation}
default-time-zone='${dbDefaultTimeZone}'
# query_cache_limit 在 MySQL 8.0.3 移除
query_cache_limit=2M
# query_cache_size 在 MySQL 8.0.3 移除
query_cache_size=64M
tmp_table_size=1024M
max_heap_table_size=1024M
join_buffer_size=1024M
thread_cache_size=32
# key_buffer_size 仅对 myisam 有效
key_buffer_size=256M
read_buffer_size=1M
read_rnd_buffer_size=128M
sort_buffer_size=1M
sql_mode="NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES"
# 安全加固
skip_symbolic_links=yes
local-infile=0
# connect_timeout=60

[mysqld_safe]
log-error=${appErrLogFile}

[mysql.server]
basedir=${appInstallDir}
EOF
            ;;
        "5.7" )
            # 安装 mysql 5.7.x 的代码段
            cat >"${appEtcFile}" <<EOF
[client]
port=${dbPort}
default-character-set=${dbCharacter}
socket=${sockFileClient}

[mysql]
default-character-set=${dbCharacter}
socket=${sockFileMysql}

[mysqld]
port=${dbPort}
character-set-server=${dbCharacter}
init_connect='SET NAMES ${dbCharacter}'
skip-character-set-client-handshake=true
basedir=${appInstallDir}
datadir=${appDataDir}
user=mysql
slow_query_log=${slowQueryLog}
# slow-query-log-file=默认为主机名-slow.log
# long_query_time=10
# log_queries_not_using_indexes=OFF
# log_output=file,table
# expire_logs_days=60
lower_case_table_names=1
explicit_defaults_for_timestamp=true
# skip-grant-tables
# skip-name-resolve
socket=${sockFileServer}
default-storage-engine=${dbEngine}
max_connections=${dbMaxConnect}
# max_connect_errors=100
max_allowed_packet=${dbMaxAllowedPacket}M
# innodb_log_files_in_group=3
innodb_log_file_size=256M
innodb_log_buffer_size=512M
innodb_buffer_pool_size=${dbIBPS%.*}M
innodb_flush_method=${innodbFlushMethod}
innodb_lock_wait_timeout=1800
innodb_flush_log_at_trx_commit=1
innodb_autoextend_increment=64
# innodb_lru_scan_depth=256
# innodb_io_capacity=
# innodb_io_capacity_max=
# innodb_buffer_pool_instances=CPU核数
# innodb_read_io_threads=64
# innodb_write_io_threads=64
# innodb_open_files=300
innodb_file_per_table=1
# innodb_file_format=Barracuda
innodb_page_size=${innodbPageSize}K
# innodb_thread_concurrency=并发线程数(官方推荐CPU核数的2倍)
# sync_binlog=0
group_concat_max_len=1024000
transaction-isolation=${dbTransactionIsolation}
default-time-zone='${dbDefaultTimeZone}'
# query_cache_limit 在 MySQL 8.0.3 移除
query_cache_limit=2M
# query_cache_size 在 MySQL 8.0.3 移除
query_cache_size=128M
tmp_table_size=1024M
max_heap_table_size=1024M
join_buffer_size=1024M
thread_cache_size=32
# key_buffer_size 仅对 myisam 有效
key_buffer_size=256M
read_buffer_size=1M
read_rnd_buffer_size=128M
sort_buffer_size=1M
sql_mode="NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES"
log_timestamps=system
# 安全加固
skip_symbolic_links=yes
local-infile=0
# connect_timeout=60

[mysqld_safe]
log-error=${appErrLogFile}

[mysql.server]
basedir=${appInstallDir}
EOF
            ;;
        "8.0" )
            # 安装 mysql 8.0.x 的代码段
            cat >"${appEtcFile}" <<EOF
[client]
port=${dbPort}
default-character-set=${dbCharacter}
socket=${sockFileClient}

[mysql]
default-character-set=${dbCharacter}
socket=${sockFileMysql}

[mysqld]
port=${dbPort}
character-set-server=${dbCharacter}
init_connect='SET NAMES ${dbCharacter}'
skip-character-set-client-handshake=true
basedir=${appInstallDir}
datadir=${appDataDir}
user=mysql
slow_query_log=${slowQueryLog}
# slow-query-log-file=默认为主机名-slow.log
# long_query_time=10
# log_queries_not_using_indexes=OFF
# log_output=file,table
# skip-name-resolve=1
lower_case_table_names=1
# expire_logs_days=60
socket=${sockFileServer}
default-storage-engine=${dbEngine}
# 字典对象缓存实时更新 此参数对性能有一定影响
# information_schema_stats_expiry=0
max_connections=${dbMaxConnect}
# max_connect_errors=100
tmp_table_size=1024M
max_heap_table_size=1024M
join_buffer_size=1024M
thread_cache_size=32
# key_buffer_size 仅对 myisam 有效
key_buffer_size=256M
read_buffer_size=1M
read_rnd_buffer_size=128M
# default_authentication_plugin=mysql_native_password
default_authentication_plugin=${dbDefaultAuthPlugin}
sql_mode="NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES"
max_allowed_packet=${dbMaxAllowedPacket}M
# innodb_log_files_in_group=3
innodb_log_file_size=256M
innodb_log_buffer_size=512M
innodb_buffer_pool_size=${dbIBPS%.*}M
innodb_flush_method=${innodbFlushMethod}
innodb_lock_wait_timeout=1800
innodb_flush_log_at_trx_commit=1
innodb_autoextend_increment=64
# innodb_lru_scan_depth=256
# innodb_io_capacity=
# innodb_io_capacity_max=
# innodb_buffer_pool_instances=CPU核数
# innodb_read_io_threads=64
# innodb_write_io_threads=64
# innodb_open_files=300
innodb_file_per_table=1
# innodb_file_format=Barracuda
innodb_page_size=${innodbPageSize}K
# innodb_thread_concurrency=并发线程数(官方推荐CPU核数的2倍)
group_concat_max_len=1024000
transaction-isolation=${dbTransactionIsolation}
default-time-zone='${dbDefaultTimeZone}'

# mysql 主从相关设置 - 主库
server-id=${hostAddr##*.}
log_bin=${hostName}-mysql-bin
# 指定 mysql 的 binlog 日志记录哪个库 缺点: 过滤操作带来的负载都在 master 上 无法做基于时间点的复制(利用 binlog)
# binlog-do-db=oadb
binlog_format=row
# max_binlog_size=256M
# 归档日志过期时间 单位秒 86400秒=1天 604800秒=7天 2592000秒=30天
binlog_expire_logs_seconds=${dbBinLogExpireLimit}
## 指定 slave 要复制哪个库 参数是在 slave 上配置 如果有多个库 需要多行列举
# replicate-do-db=oadb
replicate_wild_do_table=oadb.%
auto-increment-offset=1
auto-increment-increment=2
## 8.0.26 之前的版本使用 log_slave_updates
log_replica_updates=1
sync_binlog=1
## 8.0.26 之前的版本使用 slave_parallel_type | 8.0.29 废弃
# replica_parallel_type=LOGICAL_CLOCK
# replica_parallel_workers=4
# slave-skip-errors=all
# read-only=1
# gtid 相关设置
gtid_mode=ON
enforce_gtid_consistency=ON
skip_replica_start=ON
relay-log=${hostName}-relay-bin
# max_relay_log_size=
# relay_log_space_limit=
# relay_log_purge=ON
# relay_log_recovery=ON

# 安全加固
local-infile=0
log_timestamps=system
# connect_timeout=60

[mysqld_safe]
log-error=${appErrLogFile}

[mysql.server]
basedir=${appInstallDir}
EOF
            ;;
        * )
            oput red "脚本不支持创建当前版本的服务控制文件"
            return 1
            ;;
    esac

    if [ -f "${appEtcFile}" ]; then
        oput green "成功"
        chown mysql:mysql "${appEtcFile}"
        chmod 644 "${appEtcFile}"
    else
        oput red "失败"
        return 1
    fi

    local initDBStrs
    case "${tarFileMajorVer}.${tarFileMinorVer}" in
        "5.5" )
            # 安装 mysql 5.5.x 的代码段
            initDBStrs="${appInstallDir}/scripts/mysql_install_db \
                --user=mysql --defaults-file=${appEtcFile} \
                --basedir=${appInstallDir} \
                --datadir=${appDataDir} \
                --skip-name-resolve \
                --user=mysql"
            ;;
        "5.6" )
            # 安装 mysql 5.6.x 的代码段
            initDBStrs="${appInstallDir}/scripts/mysql_install_db \
                --user=mysql --defaults-file=${appEtcFile} \
                --basedir=${appInstallDir} \
                --datadir=${appDataDir} \
                --skip-name-resolve \
                --user=mysql"
            ;;
        "5.7" | "8.0" )
            initDBStrs="${appSbinFile} \
                --defaults-file=${appEtcFile} \
                --initialize-insecure \
                --user=mysql"
            ;;
        * )
            echo "暂时只支持 MySQL 5.5/5.6/5.7/8.0 系列的版本"
            return 1
            ;;
    esac
    echo -e "初始化库实例: \c"
    if eval "${initDBStrs}" &>>"${scriptLogFile}"; then oput green "成功"; else oput red "失败" && return 1; fi

    # 某些环境变量异常的情况下 可取消以下两行注释来使用系统默认的openssl
    # openssl_cnf="$(/usr/bin/openssl version -a | grep '^OPENSSLDIR' | awk '{print $2}' | sed 's/\"//g')/openssl.cnf"
    # export OPENSSL_CONF="${openssl_cnf}"

    # 脚本设定 开启SSL功能 只支持 5.7/8.0
    local initSSLStrs
    case "${tarFileMajorVer}.${tarFileMinorVer}" in
        "5.5" | "5.6" )
            # 不做 ssl 处理
            ;;
        "5.7" | "8.0" )
            initSSLStrs="${appBinDir}/mysql_ssl_rsa_setup --datadir=${appDataDir}"
            echo -e "生成密钥文件: \c"
            if eval "${initSSLStrs}" &>>"${scriptLogFile}"; then oput green "成功"; else oput red "失败" && return 1; fi
            ;;
    esac

    # sysvinit 和 systemd 都配置此文件 ${appInstallDir}/support-files/mysql.server
    # https://dev.mysql.com/doc/refman/8.0/en/mysql-server.html

    sed -i "s%^basedir=$%basedir=${appInstallDir}%g" "${appSrvFile}"
    echo -e "配置服务脚本: basedir=${appInstallDir} \c"
    oput green "完成"

    sed -i "s%^datadir=$%datadir=${appDataDir}%g" "${appSrvFile}"
    echo -e "配置服务脚本: datadir=${appDataDir} \c"
    oput green "完成"

    sed -i "s%mysqld_safe --datadir=%mysqld_safe --defaults-file=\"\$basedir/my.cnf\" --datadir=%" "${appSrvFile}"
    echo -e "配置服务脚本: defaults-file=${appEtcFile} \c"
    oput green "完成"
}

# 启用服务开机自启动
function set_autoStart() {
    echo -e "配置开机自启: \c"
    if [ "${initIs}" != "systemd" ]; then
        oput red "失败 当前系统不支持 systemd"
        oput blue "手动设置建议: 将以下命令加入启动项 /apps/mysql/support-files/mysql.server start"
        return 1
    fi

    if systemctl enable "${sysSrvFile##*/}" &>>"${scriptLogFile}"; then
        oput green "成功"
    else
        oput red "失败"
        return 1
    fi
}

# 写入系统环境变量
function set_envVar() {
    local envStrLD envStrPATH
    # LD_LIBRARY_PATH
    local strsLD="${appLibDir}"
    envStrLD="LD_LIBRARY_PATH=${strsLD}:\$LD_LIBRARY_PATH"
    if [ "${enableWriteEnvLD}" = "1" ]; then
        if grep -Eq "^export LD_LIBRARY_PATH=${strsLD}:\\\$LD_LIBRARY_PATH" "${sysEnvFile}"; then
            oput yellow "已存在 ${appName} 的相同 LD_LIBRARY_PATH 环境变量 无需再次写入 ${sysEnvFile}"
        else
            echo "export ${envStrLD}" >>"${sysEnvFile}"
            oput green "已写入 ${appName} 的 LD_LIBRARY_PATH 环境变量 到 ${sysEnvFile}"
        fi
    fi

    # PATH
    local strsPATH="${appBinDir}"
    envStrPATH="PATH=${strsPATH}:\$PATH"
    # 写入系统环境变量 - PATH
    if [ "${enableWriteEnvPATH}" = "1" ]; then
        if grep -Eq "^export PATH=${strsPATH}:\\\$PATH" "${sysEnvFile}"; then
            oput yellow "已存在 ${appName} 的相同 PATH 环境变量 无需再次写入 ${sysEnvFile}"
        else
            echo "export ${envStrPATH}" >>"${sysEnvFile}"
            oput green "已写入 ${appName} 的 PATH 环境变量 到 ${sysEnvFile}"
        fi
    fi
}

function get_systemInfo()
{
    # 主机
    hostName="$(hostname)"
    # CPU型号
    cpuModel="$(grep name /proc/cpuinfo | uniq | awk -F ':' '{print $(NF)}' | sed 's/^[ \t]*//g')"
    # CPU核数
    cpuLogic="$(grep -c ^processor /proc/cpuinfo 2>/dev/null)"
    # 物理内存 - 总量(低于1G的内存依旧进位1)
    memTotal="$(grep 'MemTotal' /proc/meminfo | awk '{print int($2/1024/1024+1)}')"

    # 物理磁盘
    if command -v lsblk &>/dev/null; then
        # 磁盘总量(GB)
        diskTotalSize="$(lsblk -b 2>>"${scriptLogFile}" | grep disk | awk '{total+=$4/1024/1024/1024}END{print total}')"
    elif command -v parted &>/dev/null; then
        # 磁盘总量(GB)
        diskTotalSize="$(parted -l | grep 'Disk /dev/' | grep -v 'mapper\|dm' | awk '{total+=int($3+0.5)}END{print total}')"
    else
        # 系统未安装 parted/lsblk 时 设定输出为 0
        # disk_count="0"
        diskTotalSize="0"
    fi

    # 操作系统 - 只做简单识别 懒得去把市面上的系统全都调试一遍 ^_^
    if [ -f "/etc/os-release" ]; then
        osRelease="$(awk -F '[="]+' '/^PRETTY_NAME=/{print $2}' /etc/os-release)"
    elif [ -f "/etc/redhat-release" ]; then
        osRelease="$(cat /etc/redhat-release)"
    else
        osRelease="unrecognized operating system"
    fi

    # 内核版本
    osKernelFullVer="$(uname -r | cut -d '-' -f 1)"
    osKernelMajorVer="$(uname -r | cut -d '-' -f 1 | awk -F . '{print $1}')"
    osKernelMinorVer="$(uname -r | cut -d '-' -f 1 | awk -F . '{print $2}')"

    # GNU C Library 版本
    osGlibcFullVer="$(getconf GNU_LIBC_VERSION | head -1 | awk '{print $2}')"
    # GNU C Library 主版本号
    osGlibcMajorVer="$(getconf GNU_LIBC_VERSION | head -1 | awk '{print $2}' | awk -F \. '{print $1}')"
    # GNU C Library 次版本号
    osGlibcMinorVer="$(getconf GNU_LIBC_VERSION | head -1 | awk '{print $2}' | awk -F \. '{print $2}')"
}

function get_installedVersion() {
    if [ -f "${appSbinFile}" ]; then
        installedVer="$("${appSbinFile}" --version | grep -Eo '[0-9].[0-9].[0-9]+' 2>/dev/null)"
        # 主版本号
        installedMajorVer="$(echo "${installedVer}" | cut -d '.' -f 1)"
        # 次版本号
        installedMinorVer="$(echo "${installedVer}" | cut -d '.' -f 2)"
        # 构建号
        installedBuildVer="$(echo "${installedVer}" | cut -d '.' -f 3)"
    else
        installedVer=""
        installedMajorVer=""
        installedMinorVer=""
        installedBuildVer=""
    fi
}

# 进程和端口检查
function check_pidAndPort() {
    # 进程检查 0 = 已启动 | 1 = 未启动
    # appMainPid 为主父进程PID
    # appMainPid="$(ps -ef | grep -E "${appSbinFile}" | awk "\$3==1 {print \$2}")"
    appPidNumber="$(pgrep -l mysqld | grep -E 'mysqld$' | awk '{print $1}')"
    if [ "${appPidNumber}" != "" ]; then appRunState="0"; else appRunState="1"; fi

    # 端口检测 0=已占用 1=未占用
    local checkPortCMD
    checkPortCMD="ss -lntpd | awk '{print \$5}' | grep -Eq \":${dbPort}\$\""
    # if ss -lntpd | awk '{print $5}' | grep -Eq ":${dbPort}$"; then
    if eval "${checkPortCMD}"; then
        appPortState="0"
    else
        appPortState="1"
    fi
}

# 服务管理
function manage_srv() {
    case "${action}" in
        "start" | "stop" | "restart" | "status" ) ;;
        * ) oput red "${appName} 服务管理传递参数只能是: start/stop/restart/status" && return 1; ;;
    esac
    local srvManageCMD
    case "${initIs}" in
        "systemd" )
            srvManageCMD="${srvCtlCMD} ${action} ${sysSrvFile##*/}"
            ;;
        * )
            srvManageCMD="${appSrvFile} ${action}"
            ;;
    esac

    if eval "${srvManageCMD}" &>>"${scriptLogFile}"; then
        echo -e "执行命令: ${srvManageCMD} \c"
        oput green "成功"
    else
        echo -e "执行命令: ${srvManageCMD} \c"
        oput red "失败"
        oput blue "${appName} 服务 ${action} 失败 请检查日志文件 ${scriptLogFile}"
        oput blue "可尝试使用以下命令启动: ${appSrvFile} start"
        return 1
    fi
}

# 查找 tarFile
function search_tarFile() {
    local findOpts fileSort findRegex
    findOpts="-maxdepth 1 -regextype \"posix-extended\" -regex"
    if sort --help | grep -Ewq '\-V,'; then
        fileSort="sed 's%^./%%' | sort -V | tail -1"
    else
        fileSort="sed 's%^./%%' | sort | tail -1"
    fi
    # findRegex=".*\.\/mysql.*linux.*.tar.(g|x)z$"
    findRegex=".*\.\/(mysql|Percona-Server).*(l|L)inux.*.tar.(g|x)z$"
    findCMDStr="find . ${findOpts} \"${findRegex}\" -type f | ${fileSort}"

    # echo "执行文件查找: ${findCMDStr}"
    if tarFile="$(eval "${findCMDStr}" 2>>"${scriptLogFile}")"; then
        if [ "${tarFile}" = "" ]; then return 1; fi
        # 提取二进制包的版本
        tarFileFullVer="$(echo "${tarFile}" | grep -Eo '[0-9].[0-9].[0-9]+')"
        # 主版本号
        tarFileMajorVer="$(echo "${tarFileFullVer}" | cut -d '.' -f 1)"
        # 次版本号
        tarFileMinorVer="$(echo "${tarFileFullVer}" | cut -d '.' -f 2)"
        # 构建版本
        tarFileBuildVer="$(echo "${tarFileFullVer}" | cut -d '.' -f 3)"

        # 获取二进制包文件的大小
        tarFileSize="$(du -sh "${tarFile}" | awk '{print $1}')"

        # 获取二进制包文件的后缀名
        case "${tarFile:0-7}" in
            ".tar.xz" )
                tarFileFormat="txz"
                ;;
            ".tar.gz" )
                tarFileFormat="tgz"
                ;;
        esac

        # 设定官方和非官方发行版标签
        if echo "${tarFile}" | grep -Eqi "^mysql"; then
            tarFileReleaseTag="mysql"
        elif echo "${tarFile}" | grep -Eqi "^Percona-Server"; then
            tarFileReleaseTag="percona"
        fi

        case "${tarFileReleaseTag}" in
            "mysql" )
                # 判断 MySQL 二进制包是否包含内核'linux'和GUN C库的'glibc'关键字 通过命名方式区分二进制包适应的平台环境
                if echo "${tarFile}" | grep -Eq 'linux[0-9]'; then
                    # 二进制包命名方式为: linux 以前老版本的命名方式采用内核相应版本
                    tarFileStyle="linux"
                    tarLinuxFullVer="$(echo "${tarFile}" | grep -Eo 'linux[0-9.]+' | grep -Eo '[0-9.]+')"
                    tarLinuxMajorVer="$(echo "${tarLinuxFullVer}" | awk -F . '{print $1}')"
                    tarLinuxMinorVer="$(echo "${tarLinuxFullVer}" | awk -F . '{print $2}')"
                elif echo "${tarFile}" | grep -Eq 'glibc[0-9]'; then
                    # 二进制包命名方式为: glibc 从 mysql 5.5.56 开始 采用 glibc 命名
                    tarFileStyle="glibc"
                    tarGlibcFullVer="$(echo "${tarFile}" | grep -Eo 'glibc[0-9.]+' | grep -Eo '[0-9.]+')"
                    tarGlibcMajorVer="$(echo "${tarGlibcFullVer}" | awk -F . '{print $1}')"
                    tarGlibcMinorVer="$(echo "${tarGlibcFullVer}" | awk -F . '{print $2}')"
                elif echo "${tarFile}" | grep -Eq 'minimal'; then
                    # 二进制包命名方式为: glibc + minimal 从 mysql 8.0.16 提供最小安装版 从8.0.21开始minimal包名会带上 glibc的版本
                    tarFileStyle="minimal"
                else
                    tarFileStyle="unkown"
                    # echo "包名规则: 不在脚本支持范围"
                    return 1
                fi
                ;;
            "percona" )
                # 判断 MySQL 二进制包是否包含内核'linux'和GUN C库的'glibc'关键字 通过命名方式区分二进制包适应的平台环境
                if echo "${tarFile}" | grep -Eqi 'minimal'; then
                    # 二进制包命名方式为: glibc + minimal 从 mysql 8.0.16 提供最小安装版 从8.0.21开始minimal包名会带上 glibc的版本
                    tarFileStyle="minimal"
                    tarGlibcFullVer="$(echo "${tarFile}" | grep -Eo 'glibc[0-9.]+' | grep -Eo '[0-9.]+')"
                    tarGlibcMajorVer="$(echo "${tarGlibcFullVer}" | awk -F . '{print $1}')"
                    tarGlibcMinorVer="$(echo "${tarGlibcFullVer}" | awk -F . '{print $2}')"
                elif echo "${tarFile}" | grep -Eq 'glibc[0-9]'; then
                    tarFileStyle="glibc"
                    tarGlibcFullVer="$(echo "${tarFile}" | grep -Eo 'glibc[0-9.]+' | grep -Eo '[0-9.]+')"
                    tarGlibcMajorVer="$(echo "${tarGlibcFullVer}" | awk -F . '{print $1}')"
                    tarGlibcMinorVer="$(echo "${tarGlibcFullVer}" | awk -F . '{print $2}')"
                else
                    tarFileStyle="unkown"
                    # echo "包名规则: 不在脚本支持范围"
                    return 1
                fi
                ;;
        esac
        # echo "文件查找结果: 已找到文件 ${tarFile} - ${tarFileSize}"
    else
        # echo "文件查找命令: 执行失败 ${findCMDStr}"
        # echo "脚本日志文件: ${scriptLogFile}"
        return 1
    fi
}

# 解压 tarFile
function unpack_tarFile() {
    local ucp
    # 解包后的目录相对路径 ucp=use-compress-program
    case "${tarFileFormat}" in
        "tgz" )
            ucp="gzip"
            unpackDir="${tarFile%.tar.gz}"
            ;;
        "txz" )
            ucp="xz"
            unpackDir="${tarFile%.tar.xz}"
            ;;
        * )
            unpackDir=""
            oput red "解包失败: 二进制安装包文件后缀名必须是 tar.xz 或 tar.gz"
            return 1
            ;;
    esac
    if ! command -v "${ucp}" &>/dev/null; then oput red "解包失败: ${ucp} 命令未找到" && return 1; fi

    # 如果解包目录已存在 则删除
    if [ -d "${installRootDir}/${unpackDir}" ] && [ "${unpackDir}" != "" ]; then
        echo -e "删除目录: ${installRootDir}/${unpackDir} \c"
        if rm -rf "${installRootDir:?parameter null or not set}/${unpackDir:?parameter null or not set}"; then
            oput green "成功"
        else
            echo "失败"
            return 1
    fi
    fi
    # 解压命令
    unpackCMD="tar -xf ${tarFile} -C ${installRootDir}"
    # 如果需要手动指定调用的解压程序 则按需调整后 使用下行解压命令
    # unpackCMD="tar --use-compress-program=xz -xf ${tarFile} -C ${installRootDir}"

    # 解压二进制包文件
    echo -e "正在解包: ${tarFile} \c"
    if eval "${unpackCMD}" 2>>"${scriptLogFile}"; then
        oput green "成功"
    else
        oput red "失败"
        return 1
    fi
}

# 版本限制
function check_versionLimit() {
    # 软件层面
    # 版本限制参考 https://www.mysql.com/support/supportedplatforms/database.html
    # 限制 MySQL 版本号必须等于 5.5/5.6/5.7/8.0

    # 受支持的官方版和Percona版
    case "${tarFileReleaseTag}" in
        "mysql" )
            case "${tarFileMajorVer}.${tarFileMinorVer}" in
                5.[5-7] | "8.0" )
                    ;;
                * )
                    oput red "当前版本: MySQL ${tarFileFullVer} 不是 MySQL 5.5/5.6/5.7/8.0"
                    return 1
                    ;;
            esac
            ;;
        "percona" )
            case "${tarFileMajorVer}.${tarFileMinorVer}" in
                "5.7" | "8.0" )
                    ;;
                * )
                    oput red "当前版本: Percona Server for MySQL ${tarFileFullVer} 的版本不是 MySQL 5.7/8.0"
                    return 1
                    ;;
            esac
            ;;
        * )
            oput red "脚本只支持 mysql 和 percona" && return 1
            ;;
    esac

    echo -e "版本检查: \c"
    case "${tarFileMajorVer}.${tarFileMinorVer}" in
        # MySQL 5.x 系列
        "5.5" )
            # 5.5.x 限制最低版本号为 5.5.8
            if [ "${tarFileBuildVer}" -ge 8 ] ; then
                oput green "当前 ${tarFileFullVer} >= 预期 5.5.8 - 已满足条件"
            else
                oput red "当前 ${tarFileFullVer} < 预期 5.5.8 - 未满足条件"
                return 1
            fi
            ;;
        "5.6" )
            # 5.6.x 限制最低版本号为 5.6.10
            if [ "${tarFileBuildVer}" -ge 10 ] ; then
                oput green "当前 ${tarFileFullVer} >= 预期 5.6.10 - 已满足条件"
            else
                oput red "当前 ${tarFileFullVer} < 预期 5.6.10 - 未满足条件"
                return 1
            fi
            ;;
        "5.7" )
            case "${tarFileReleaseTag}" in
                "mysql" )
                    # 5.7.x 限制最低版本号为 5.7.9
                    if [ "${tarFileBuildVer}" -ge 9 ] ; then
                        oput green "当前 ${tarFileFullVer} >= 预期 5.7.9 - 已满足条件"
                    else
                        oput red "当前 ${tarFileFullVer} < 预期 5.7.9 - 未满足条件"
                        return 1
                    fi
                    ;;
                "percona" )
                    # 5.7.x 限制最低版本号为 5.7.31
                    if [ "${tarFileBuildVer}" -ge 31 ] ; then
                        oput green "当前 ${tarFileFullVer} >= 预期 5.7.31 - 已满足条件"
                    else
                        oput red "当前 ${tarFileFullVer} < 预期 5.7.31 - 未满足条件"
                        return 1
                    fi
                    ;;
                * )
                    oput red "脚本只支持 mysql 和 percona" && return 1
                    ;;
            esac
            ;;
        "8.0" )
            case "${tarFileReleaseTag}" in
                "mysql" )
                    # 8.0.x 限制最低版本号为 8.0.11
                    if [ "${tarFileBuildVer}" -ge 11 ] ; then
                        oput green "当前 ${tarFileFullVer} >= 预期 8.0.11 - 已满足条件"
                    else
                        oput red "当前 ${tarFileFullVer} < 预期 8.0.11 - 未满足条件"
                        return 1
                    fi
                    ;;
                "percona" )
                    # 8.0.x 限制最低版本号为 8.0.20
                    if [ "${tarFileBuildVer}" -ge 20 ] ; then
                        oput green "当前 ${tarFileFullVer} >= 预期 8.0.20 - 已满足条件"
                    else
                        oput red "当前 ${tarFileFullVer} < 预期 8.0.20 - 未满足条件"
                        return 1
                    fi
                    ;;
                * )
                    oput red "脚本只支持 mysql 和 percona" && return 1
                    ;;
            esac
            ;;
        * )
            oput red "当前 ${tarFileFullVer} != 预期 5.5/5.6/5.7/8.0 - 未满足条件"
            return 1
            ;;
    esac

    # 限制 内核版本 或 GUN C 库的版本
    case "${tarFileStyle}" in
        "linux" )
            echo -e "内核检查: \c"
            # 如果OS的kernel版本 小于 mysql的kernel版本 则不满足安装条件
            if [ "${tarLinuxMajorVer}" -eq "${osKernelMajorVer}" ] ; then
                if [ "${osKernelMinorVer}" -lt "${tarLinuxMinorVer}" ] ; then
                    oput red "当前 ${tarLinuxFullVer} > 系统 ${osKernelFullVer} - 未满足条件"
                    return 1
                else
                    oput green "当前 ${tarLinuxFullVer} <= 系统 ${osKernelFullVer} - 已满足条件"
                fi
            else
                if [ "${osKernelMajorVer}" -gt "${tarLinuxMajorVer}" ] ; then
                    oput green "当前 ${tarLinuxFullVer} <= 系统 ${osKernelFullVer} - 已满足条件"
                else
                    oput red "当前 ${tarLinuxFullVer} > 系统 ${osKernelFullVer} - 未满足条件"
                    return 1
                fi
            fi
            ;;
        "glibc" )
            echo -e "C 库检查: \c"
            # 如果OS的glibc版本 小于 mysql的glibc版本 则不满足安装条件
            if [ "${tarGlibcMajorVer}" -eq "${osGlibcMajorVer}" ] ; then
                if [ "${osGlibcMinorVer}" -lt "${tarGlibcMinorVer}" ] ; then
                    oput red " 当前 ${tarGlibcFullVer} > 系统 ${osGlibcFullVer} - 未满足条件"
                    return 1
                else
                    oput green "当前 ${tarGlibcFullVer} <= 系统 ${osGlibcFullVer} - 已满足条件"
                fi
            else
                oput red "当前 ${tarGlibcFullVer} != 系统 ${osGlibcFullVer} - 未满足条件"
                return 1
            fi
            ;;
        "minimal" )
            echo -e "C 库检查: \c"
            case "${tarFileReleaseTag}" in
                "mysql" )
                    case "${tarFileMajorVer}.${tarFileMinorVer}" in
                        # MySQL 8.0.x 系列 的二进制版（最小安装版）要求最低是 glibc 2.14
                        "8.0" )
                            # 比较浮点数大小
                            if [ "$(echo "${osGlibcFullVer} 2.14" | awk '{if ($1>=$2) {print 0}}')" = "0" ]; then
                                oput green "系统 ${osGlibcFullVer} >= 预期 2.14 - 已满足条件"
                            else
                                oput red "系统 ${osGlibcFullVer} < 预期 2.14 - 未满足条件"
                                return 1
                            fi
                            ;;
                    esac
                    ;;
                "percona" )
                    case "${tarFileMajorVer}.${tarFileMinorVer}" in
                        # percona MySQL 8.0.x 系列 的二进制版（最小安装版）要求最低是 glibc 2.12
                        "8.0" )
                            # 比较浮点数大小
                            if [ "$(echo "${osGlibcFullVer} 2.12" | awk '{if ($1>=$2) {print 0}}')" = "0" ]; then
                                oput green "系统 ${osGlibcFullVer} >= 预期 2.12 - 已满足条件"
                            else
                                oput red "系统 ${osGlibcFullVer} < 预期 2.12 - 未满足条件"
                                return 1
                            fi
                            ;;
                    esac
                    ;;
                * )
                    oput red "脚本只支持 mysql 和 percona" && return 1
                    ;;
            esac
            ;;
    esac
}

#
function select_installMode() {
    local isInstalled inputSelect
    if [ -d "${appInstallDir}" ]; then
        isInstalled="yes"
        echo; echo "已有安装检查: 已检测到 ${appName} 的同名安装目录 ${appInstallDir}"
        case "${appRunState}" in
            "0" )
                echo "检查服务状态: ${appName} 服务已启动 PID=${appPidNumber}"
                ;;
            "1" )
                if [ -f "${appSbinFile}" ]; then
                    echo "检查服务状态: ${appName} 服务未启动"
                else
                    echo "检查服务状态: 未能判断服务状态 未找到文件 ${appSbinFile}"
                fi
                ;;
        esac
    else
        isInstalled="no"
    fi

    case "${isInstalled}" in
        "yes" )
            echo
            echo "d = 删除且全新安装 | u = 备份后覆盖安装 | 其他键 = 取消安装并退出"
            echo
            read -rp "请输入你的选择: " inputSelect
            echo

            # 根据输入定义安装模式 new=全新安装 cover=覆盖安装(只更新程序而不删除配置文件)
            case "${inputSelect}" in
                [dD] )
                    installMode="new"
                    echo "全新安装 ${appName} 将删除同名安装目录"
                    ;;
                [uU] )
                    installMode="cover"
                    echo "覆盖安装 ${appName}"
                    ;;
                * )
                    echo "安装取消 脚本退出"
                    echo && return 1
                    ;;
            esac

            # 当选择删除和更新安装时 都杀掉服务进程
            case "${inputSelect}" in
                [dD] | [uU] )
                    # 杀进程
                    if ! kill_appPid; then return 1; fi
                    ;;
            esac
            ;;
        "no" )
            installMode="new"
            ;;
    esac
}

function kill_appPid() {
    # 杀进程
    if [ "${appRunState}" = "0" ]; then
        if kill "${appPidNumber}"; then
            echo; echo "kill ${appName}d main process pid ${appPidNumber} succeed"
        else
            echo; echo "kill ${appName}d main process pid ${appPidNumber} failed"
            return 1
        fi
        # pkill mysqld
    fi
    sleep 3
}

# 执行备份
function backup_app() {
    if [ "${appName}" = "" ]; then oput red "执行备份: 失败 未指定软件名" && return 1; fi
    echo; echo -e "执行备份: ${installRootDir}/${appName} \c"
    # 执行备份(解压成功才进行备份)
    if [ ! -d "${installRootDir}/${appName}" ]; then
        oput yellow "失败 目录不存在 无需备份"
        echo && return 1
    else
        oput blue "使用 ${backupFormat} 文件格式打包压缩"
    fi

    local backupFile backupCMD showpackCMD
    # 备份文件名
    backupFile="${backupDir}/backup-${appName}-$(date '+%Y%m%d%H%M%S').${backupFormat}"
    case "${backupFormat}" in
        "tgz" )
            # 打包命令
            backupCMD="tar -czvf ${backupFile} -C ${installRootDir} ${appName} &>>${scriptLogFile}"
            # 查包命令
            showpackCMD="tar -tzf ${backupFile}"
            # 解包命令
            unpackCMD="tar -xzf ${backupFile}"
            ;;
        "rar" )
            # 打包命令
            backupCMD="rar a -ep1 ${backupFile} ${installRootDir}/${appName} &>>${scriptLogFile}"
            # 查包命令
            showpackCMD="rar v ${backupFile} 或 unrar l ${backupFile}"
            # 解包命令
            unpackCMD="rar x -o+ -inul ${backupFile}"
            ;;
        "zip" )
            # 打包命令
            backupCMD="cd ${installRootDir} && zip -r ${backupFile} ${appName} &>>${scriptLogFile}"
            # 查包命令
            showpackCMD="zip -sf ${backupFile} 或 unzip -l ${backupFile}"
            # 解包命令
            unpackCMD="unzip -oq ${backupFile} 或 7za x -y ${backupFile}"
            ;;
        "7z" )
            # todo
            # 打包命令
            backupCMD=""
            # 查包命令
            showpackCMD=""
            # 解包命令
            unpackCMD=""
            echo "暂不支持 7z 格式" && return 1
            ;;
    esac
    echo -e "打包命令: \c"
    oput blue "${backupCMD}"
    echo -e "执行备份: \c"
    if eval "${backupCMD}" &>>"${scriptLogFile}"; then
        oput green "成功 文件: ${backupFile} 大小: $(du -sh "${backupFile}" | awk '{print $1}')"
        oput blue "查看命令: ${showpackCMD}"
        oput blue "解压命令: ${unpackCMD}"
    else
        oput red "失败 日志文件: ${scriptLogFile}"
        return 1
    fi

    case "${backupFormat}" in
        "zip" )
            # zip 方式备份时 需要切换回脚本文件所在目录
            if ! cd "${scriptDir}"; then
                echo "目录切换: 失败 ${scriptDir}" && return 1
            fi
            ;;
    esac
}

function msg_depend() {
    cat <<EOF

依赖安装参考
  RedHat 5.x: yum install -y iproute ncurses perl libaio util-linux tar gzip numactl
  RedHat 6.x: yum install -y iproute ncurses-libs perl libaio util-linux-ng tar gzip numactl
  RedHat 7.x: yum install -y iproute ncurses-libs perl perl-Data-Dumper libaio util-linux tar gzip numactl libatomic
  RedHat 8.x: dnf install -y iproute perl perl-Data-Dumper libaio util-linux tar gzip numactl libatomic ncurses-compat-libs
  RedHat 9.x: dnf install -y iproute perl perl-Data-Dumper libaio util-linux tar gzip numactl libatomic libxcrypt-compat
  Fedora 23-29: dnf install -y iproute perl-Data-Dumper perl-Getopt-Long libaio gzip numactl-libs
  Fedora 30+: dnf install -y iproute perl-Data-Dumper perl-Getopt-Long libaio gzip numactl-libs compat-openssl10
  Suse 11: zypper in -y iproute2 libncurses5 util-linux perl libaio gzip numactl
  Suse 12+: zypper in -y iproute2 libncurses5 util-linux perl libaio1 gzip numactl libatomic1
  Suse Leap 15+: zypper in -y iproute2 util-linux perl libaio1 gzip numactl
  Debian/UOS: apt install -y iproute2 libncurses5 util-linux perl libaio1 gzip libnuma1 libatomic1
  Ubuntu/UKylin/Deepin: apt install -y iproute2 libncurses5 util-linux perl libaio1 gzip numactl libatomic1
其他一些情况
  报错: error while loading shared libraries: libtinfo.so.5: cannot open shared object file: No such file or directory
  解决: 执行 find / -name libncurses.so* 根据查找结果创建 libncurses.so.x.x 到同目录下 libtinfo.so.5 的软连接
  例如: cd /usr/lib64 && ln -s libncurses.so.6.2 libncurses.so.5 && ln -s libtinfo.so.6.2 libtinfo.so.5

EOF
}

# 成功安装后的提示信息
function msg_OK() {
    echo
    case "${tarFileMajorVer}.${tarFileMinorVer}" in
        5.[5-6] )
            echo "MySQL安装完成 记得按需调整运行参数: innodb_buffer_pool_size 和 innodb_additional_mem_pool_size"
            ;;
        "5.7" | "8.0" )
            echo "MySQL安装完成 记得按需调整运行参数: innodb_buffer_pool_size"
            ;;
    esac
    # innodb_buffer_pool_size 调优计算方法参考：
    # val = Innodb_buffer_pool_pages_data / Innodb_buffer_pool_pages_total * 100%
    # val > 95% 则考虑增大 建议使用物理内存的75%
    # val < 95% 则考虑减小 建议设置为：Innodb_buffer_pool_pages_data * Innodb_page_size * 1.05 / (1024*1024*1024)
    # 设置命令：set global innodb_buffer_pool_size = 2097152; // 缓冲池字节大小 单位kb 默认128M

    echo
    echo "安装信息"
    echo "安装路径: ${appInstallDir}"
    echo "当前版本: ${tarFileFullVer}"
    echo "root密码: ${dbRandPWD}"
    echo "远程登录: 已配置 root 帐号可以从任意主机上登录"
    if [ "${tarFileMajorVer}.${tarFileMinorVer}" = "8.0" ]; then
        echo "登录认证: 已配置 root 帐号认证为 ${rootAuthPlugin}"
    fi
    echo "连接命令: ${appBinFile} -h${hostAddr} -P${dbPort} -uroot -p${dbRandPWD}"

    local sqlChangePWD
    case "${tarFileMajorVer}.${tarFileMinorVer}" in
        "5.5" | "5.6" | "5.7" )
            sqlChangePWD="set password for 'root'@'%' = password('i4Seeyon'); flush privileges;"
            ;;
        "8.0" )
            sqlChangePWD="alter user 'root'@'%' identified with mysql_native_password by 'i4Seeyon'; flush privileges;"
            ;;
    esac
    echo "修改密码: ${sqlChangePWD}"
    echo
    echo "命令参考"
    case "${initIs}" in
        "systemd" )
            echo "启动: systemctl start mysqld"
            echo "关闭: systemctl stop mysqld"
            echo "重启: systemctl restart mysqld"
            echo "状态: systemctl status mysqld"
            ;;
        * )
            echo "启动: ${appSrvFile} start"
            echo "关闭: ${appSrvFile} stop"
            echo "重启: ${appSrvFile} restart"
            echo "状态: ${appSrvFile} status"
            ;;
    esac
}

# 安装软件
function install_app() {
    # 软件名检查
    if [ "${appName}" = "" ]; then echo "未设置软件名 不能执行安装" && return 1; fi

    # 目录切换
    if ! cd "${scriptDir}" 2>>"${scriptLogFile}"; then echo "目录切换: 失败 ${scriptDir}" && return 1; fi

    echo
    echo "解压文件(${appName})"
    if ! unpack_tarFile; then return 1; fi

    if [ "${enableBackup}" = "1" ]; then
        if [ -d "${appInstallDir}" ]; then
            if ! backup_app; then return 1; fi
        fi
    fi

    echo
    echo -e "删除目录: ${appInstallDir} \c"
    if rm -rf "${appInstallDir}"; then
        oput green "成功"
    else
        oput red "失败"
        return 1
    fi
    # do not prompt before overwriting
    echo -e "移动目录: ${installRootDir}/${unpackDir} to ${appInstallDir} \c"
    if mv "${installRootDir}/${unpackDir}" "${appInstallDir}"; then
        oput green "成功"
    else
        oput red "失败"
        return 1
    fi

    # 系统组和用户
    echo; echo "创建系统用户/组"
    if ! create_groupAndUser; then return 1; fi

    # 安全加固要求有mysql日志记录
    if [ ! -d "${appLogDir}" ]; then mkdir -p "${appLogDir}"; fi
    if [ ! -f "${appErrLogFile}" ]; then touch "${appErrLogFile}"; fi

    echo -e "配置目录属主: chown -R mysql:mysql ${appInstallDir} \c"
    if chown -R mysql:mysql "${appInstallDir}" ; then oput green "成功"; else echo "失败" && return 1; fi
    echo -e "配置目录权限: chmod -R 755 ${appInstallDir} \c"
    # 如果设置为 750 其他用户不能访问
    if chmod -R 755 "${appInstallDir}"; then oput green "成功"; else echo "失败" && return 1; fi

    # 设置参数
    echo; echo "参数设置(${appName})"
    if ! set_appPara; then exit; fi

    # 环境变量
    echo; echo "环境配置(${appName})"
    set_envVar

    # 创建服务控制文件
    echo; echo "配置服务(${appName})"
    create_sysSrvFile

    # 配置开机自启动
    if [ "${enableAutostart}" = "1" ]; then
        echo; echo "自启设置(${appName})"
        set_autoStart
    else
        echo "依照脚本参数设定 不启用 ${appName} 的开机自启动服务"
    fi

    echo; echo "启动服务(${appName})"
    action="start"; if ! manage_srv; then return 1; fi

    local localConnStrs dbSetRootStrs
    localConnStrs="${appBinFile} -h127.0.0.1 -P${dbPort}"

    dbRandPWD="$(export LC_ALL=C; tr -cd 0-9a-zA-Z < /dev/urandom | head -c "${dbPWDDigit}")"
    echo -e "生成随机密码: \c"
    if [ "${dbRandPWD}" = "" ]; then oput red "失败" && return 1; else oput green "成功"; fi

    case "${tarFileMajorVer}.${tarFileMinorVer}" in
        5.[5-6] )
            # 安装 mysql 5.5.x/5.6.x 的代码段
            dbSetRootStrs="${localConnStrs} -e \
                \"use mysql; \
                update user set password=PASSWORD('${dbRandPWD}') where host='localhost' and user='root'; \
                delete from user where password =''; \
                update user set host='%' where host='localhost' and user='root'; \
                flush privileges;\""
            ;;
        "5.7" )
            # 安装 mysql 5.7.x 的代码段
            dbSetRootStrs="${localConnStrs} -e \
                \"use mysql; \
                update mysql.user set authentication_string=password('${dbRandPWD}') \\
                   where user='root' and host='localhost'; \
                update user set host='%' where host='localhost' and user='root'; \
                flush privileges;\""
            ;;
        "8.0" )
            # 安装 mysql 8.0.x 的代码段
            # 设置root帐户的身份认证方式 这个设置不受配置文件的影响 可按需修改 这里设置的目的是为了兼容性
            rootAuthPlugin="mysql_native_password"
            dbSetRootStrs="${localConnStrs} -e \
                \"use mysql;\
                alter user 'root'@'localhost' identified with '${rootAuthPlugin}' by '${dbRandPWD}';\
                update user set host='%' where user='root';\
                flush privileges;\""
            ;;
    esac

    # 设置 binlog_expire_logs 过期清理时间 不管配置文件中是否启用 bin-log 都进行设置
    #    case "${tarFileMajorVer}.${tarFileMinorVer}" in
    #        "8.0" )
    #            setBinLogStrs="${localConnStrs} -e \"set persist binlog_expire_logs_seconds=${dbBinLogExpireLimit};\""
    #            # set global binlog_expire_logs_seconds=60*60*24;
    #            echo -e "额外参数调整: 设置 binlog_expire_logs_seconds = ${dbBinLogExpireLimit} \c"
    #            if eval "${setBinLogStrs}" &>>"${scriptLogFile}"; then
    #                oput green "成功"
    #            else
    #                echo "失败"
    #                return 1
    #            fi
    #            ;;
    #    esac

    echo -e "配置root帐户: \c"
    if eval "${dbSetRootStrs}" &>>"${scriptLogFile}"; then
        oput green "成功"
    else
        oput red "失败"
        return 1
    fi

    echo; echo "重启服务(${appName})"
    action="restart"; if ! manage_srv; then return 1; fi

    local remoteConnStrs select_v_str createDBStrs
    remoteConnStrs="${appBinFile} -h${hostAddr} -P${dbPort} -uroot -p${dbRandPWD}"
    echo -e "远程连接测试: \c"
    # 判断MySQL服务是否正常
    if eval "${appAdminBinFile}" -h"${hostAddr}" -uroot -p"${dbRandPWD}" ping &>>"${scriptLogFile}"; then
        oput green "成功"
        select_v_str="select now(),user(),version();"
        echo -e "SQL 语句测试: \c"
        if eval "${remoteConnStrs}" -e "\"${select_v_str}\"" &>>"${scriptLogFile}"; then
            oput green "成功"
        else
            echo "失败"
            return 1
        fi
    else
        echo "失败 命令执行异常"
        return 1
    fi

    if [ "${createDBName}" != "" ]; then
        createDBStrs="create database if not exists ${createDBName} character set utf8 collate utf8_general_ci;"
        echo -e "创建新数据库: ${createDBName} \c"
        if eval "${remoteConnStrs}" -e "\"${createDBStrs}\"" &>>"${scriptLogFile}"; then
            oput green "成功"
        else
            echo "失败"
            return 1
        fi
    fi

    # 关闭 mysql 服务
    if [ "${startAppSrv}" != "1" ]; then
        action="stop"; if ! manage_srv; then return 1; fi
    fi

    msg_OK
}

# 升级
function update_app() {
    if [ "${appName}" = "" ]; then echo "未设置软件名 不能执行安装" && return 1; fi
    if ! cd "${scriptDir}" 2>>"${scriptLogFile}"; then echo "目录切换: 失败 ${scriptDir}" && return 1; fi

    get_installedVersion

    echo; echo -e "版本比较: \c"
    if [ "${tarFileMajorVer}.${tarFileMinorVer}" != "${installedMajorVer}.${installedMinorVer}" ]; then
        oput red "待装/已装 的主次版本号未匹配 ${tarFileFullVer} != ${installedVer}"
        return 1
    else
        local tempStrs="${tarFileMajorVer}.${tarFileMinorVer} = ${installedMajorVer}.${installedMinorVer}"
        oput green "待装/已装 的主次版本号已匹配 ${tempStrs}"
        echo -e "版本比较: \c"
        # 如果要允许降级覆盖安装 可以注释后续条件判断
        if [ "${tarFileBuildVer}" -lt "${installedBuildVer}" ]; then
            oput red "待装/已装 的版本构建号未满足 当前 ${tarFileFullVer} < ${installedVer}"
            return 1
        else
            oput green "待装/已装 的版本构建号已满足 ${tarFileBuildVer} >= ${installedBuildVer}"
        fi
    fi

    echo
    echo "解压文件(${appName})"
    if ! unpack_tarFile; then return 1; fi
    # 覆盖安装的情况下 检测二进制包是不是存在 data/ my.cnf support-files/mysql.server 如果存在则删除
    local lists=(
        "${installRootDir}/${unpackDir}/data"
        "${installRootDir}/${unpackDir}/my.cnf"
        "${installRootDir}/${unpackDir}/support-files/mysql.server"
        )
    for x in "${lists[@]}"; do
        if [ -d "${x}" ]; then
            rm -rf "${x}"
        elif [ -f "${x}" ]; then
            rm -f "${x}"
        fi
    done

    if [ "${enableBackup}" = "1" ]; then
        if [ -d "${appInstallDir}" ]; then
            echo -e "备份提示: \c"
            oput blue "如果数据库占用过大 请手动备份后 关闭脚本的自动备份 以期节约备份时间"
            if ! backup_app; then return 1; fi
        fi
    fi

    # do not prompt before overwriting
    echo
    echo "复制文件: ${installRootDir}/${unpackDir}/* to ${appInstallDir}/"
    alias cp='cp'
    local cpCMD
    cpCMD="cp -af ${installRootDir}/${unpackDir}/* ${appInstallDir}/"
    echo -e "执行命令: ${cpCMD} \c"
    if eval "${cpCMD}"; then oput green "成功"; else echo "失败" && return 1; fi

    echo "删除目录: ${installRootDir}/${unpackDir}"
    rm -rf "${installRootDir:?parameter null or not set}/${unpackDir:?parameter null or not set}"

    echo -e "目录属主: \c"
    if chown -R mysql:mysql "${appInstallDir}" ; then oput green "配置成功"; else echo "配置失败" && return 1; fi
    echo -e "目录权限: \c"
    if chmod -R 755 "${appInstallDir}"; then oput green "配置成功"; else echo "配置失败" && return 1; fi

    echo; echo "已覆盖原有的安装目录 请参照官网对应版本的文档进行后续操作"
    echo "对于 MySQL 8.0.16 之前的版本 可能需要执行 mysql_upgrade"
}

# 移除
function remove_app() {
    # 执行删除
    if [ -d "${appInstallDir}" ]; then
        echo; echo "执行卸载(${appName})"
    else
        echo; echo "执行卸载(${appName})"
        echo "卸载失败: 目录不存在 ${appInstallDir}"; echo && return 1
    fi

    # 防止误删 根据脚本参数设置 启用删除前的自动备份
    if [ "${enableBackup}" = "1" ]; then
        if ! backup_app; then return 1; fi
    fi

    echo
    echo "清除开机自启"
    local willDelFiles
    willDelFiles=(
        "/usr/lib/systemd/system/mysqld.service"
        "/lib/systemd/system/mysqld.service"
        "/etc/systemd/system/mysqld.service"
        )
    for willDelFile in "${willDelFiles[@]}"; do
        # RedHat/Suse: /usr/lib/systemd/system/mysqld.service
        # Ubuntu/Deepin: /lib/systemd/system/mysqld.service
        if [ -f "${willDelFile}" ]; then
            if systemctl disable "${willDelFile##*/}" &>>"${scriptLogFile}"; then
                echo "已禁用 开机自启动"
            fi
            rm -f "${willDelFile}"
            echo "已删除 自启动文件 ${willDelFile}"
        fi
    done

    echo "清理环境变量"
    if grep -q "^# set mysql" "${sysEnvFile}"; then
        sed -i "/^# set mysql/d" "${sysEnvFile}"
        echo "已清理 MySQL 环境变量: # set mysql"
    fi
    if grep -q "^export PATH=${appBinDir}:\$PATH" "${sysEnvFile}"; then
        sed -i "/^export.*mysql\/bin:\$PATH/d" "${sysEnvFile}"
        echo "已清理 MySQL 环境变量: export PATH=${appBinDir}:\$PATH"
    fi

    echo "清除安装目录"
    if rm -rf "${appInstallDir}"; then
        echo "已删除 MySQL 安装目录: ${appInstallDir}"
    else
        echo "未删除 MySQL 安装目录: ${appInstallDir}"
        return 1
    fi
}

# 打印主机相关信息
function print_host_info() {
    echo "登录信息: $(id -un)@${hostAddr} from ${clientAddr}"
    echo "主机名称: ${hostName}"
    echo "CPU 型号: ${cpuModel}"
    echo "硬件概览: ${cpuLogic} 核CPU + ${memTotal} GB内存 + ${diskTotalSize:=0} GB磁盘"
    echo "操作系统: ${osRelease}"
    echo "Bash版本: ${BASH_VERSION}"
    echo "系统内核: ${osKernelFullVer}"
    echo "GNU C 库: ${osGlibcFullVer}"
}

# 打印脚本信息
function print_scriptInfo() {
    echo
    echo "脚本文件: ${scriptFile}"
    echo "脚本功能: ${scriptDesc}"
    echo "系统支持: ${scriptEnv}"
    echo "脚本用法: ${scriptUsage}"
    echo "更新时间: ${scriptUpdate}"
    echo
    print_host_info

    echo
    if [ "${appRunState}" = "0" ]; then
        echo "进程检查: 已检测到 PID=${appPidNumber}"
    else
        echo "进程检查: 未检测到"
    fi
    if [ "${appPortState}" = "0" ]; then
        echo "监听端口: 已被占用 ${dbPort}"
    else
        echo "监听端口: 未被占用 ${dbPort}"
    fi
    if [ "${tarFile}" != "" ]; then
        echo "二进制包: 已找到 ${tarFile} - ${tarFileSize}"
    else
        echo "二进制包: 未找到 请将从官网下载的二进制压缩包与脚本放在一起"
        echo "查找命令: ${findCMDStr}"
    fi
    if [ "${installRootDir}" != "" ]; then
        echo "安装目录: ${installRootDir}/mysql"
    else
        echo "安装目录: 未设定安装根目录"
    fi
}

# 未传参时的脚本提示
function main_prompt() {
    # 输出信息提示
    cat <<EOF

MySQL版本要求
  Official: 5.5.x >= 5.5.8 | 5.6.x >= 5.6.10 | 5.7.x >= 5.7.9 | 8.0.x >= 8.0.11
  Percona : 5.7.x >= 5.7.31 | 8.0.x >= 8.0.20
脚本使用说明
  只支持 x86_64/AMD64 的操作系统以及官方的二进制安装包
  脚本和 MySQL 二进制包放在同级目录 终端下传参执行脚本
脚本相关命令
  安装: bash ${scriptFile} install
  卸载: bash ${scriptFile} remove
  备份: bash ${scriptFile} backup
  依赖: bash ${scriptFile} depend
服务启停管理
  systemd: systemctl start|restart|stop|status mysqld
  manual : ${installRootDir}/mysql/support-files/mysql.server start|restart|stop|status
官方下载地址
  5.5.x: https://dev.mysql.com/downloads/mysql/5.5.html
  5.6.x: https://dev.mysql.com/downloads/mysql/5.6.html
  5.7.x: https://dev.mysql.com/downloads/mysql/5.7.html
  8.0.x: https://dev.mysql.com/downloads/mysql/8.0.html

EOF

oput red "安装前 请先执行依赖检查"

}

appName="mysql"

get_systemInfo

check_pidAndPort
search_tarFile

print_scriptInfo

if ! verify_scriptPara; then exit; fi
if ! check_CMD; then exit; fi

case "$1" in
    "install" )
        if [ "${tarFile}" = "" ]; then echo; exit; fi
        if ! check_versionLimit; then echo; exit; fi
        echo; print_parameter_info
        if ! select_installMode; then exit; fi
        case "${installMode}" in
            "new" )
                disable_selinux
                if ! install_app 2>&1 | tee "${scriptLogFile}"; then exit; fi
                ;;
            "cover" )
                if ! update_app 2>&1 | tee "${scriptLogFile}"; then exit; fi
        esac
        echo; echo "脚本运行日志: ${scriptLogFile}"
        ;;
    "remove" )
        if [ -d "${appInstallDir}" ]; then
            if ! kill_appPid; then exit; fi
            remove_app
        else
            echo; echo "卸载失败: 目录不存在 ${appInstallDir}"
        fi
        ;;
    "backup" )
        if [ -d "${appInstallDir}" ]; then
            if ! kill_appPid; then exit; fi
            backup_app
        else
            echo; echo "备份失败: 目录不存在 ${appInstallDir}"
        fi
        ;;
    "depend" )
        msg_depend
        ;;
    * )
        main_prompt
        ;;
esac

echo
